require("bixbyte-frame");
app.port = 3000;

mongoose.Promise = global.Promise;

//! CONNECT TO THE DATABASE
db("mongo", "bixbyteSMS")
.then(function(){


    //! MIDLEWARE SETUP
    app.use( express.static( __dirname + '/') ); 

    var connUsers = 0;
    var onlineClients = [];
    
    //! CREATE A SOCKET CONNECTION  
    io.on('connection', function(socket){
      
      /**
       * PERFORM BASIC AUTHENTICATION FOR THE SMS API
       */

    connUsers++;
        
    console.log("\nConnected to ".info + connUsers + ( (connUsers > 1 || connUsers == 0 ) ? " clients.".info : " client.".info)  + "\t +".success );
      
    socket.emit("onlineClients",onlineClients)

    //! HANDLE REQUESTS FOR A LIST OF SMS' AND THEIR STATUS
    socket.on("getStatus", function(){
        
          schema.SMS.getStatus()
          .then(function(d){
          // console.log(d.response);
              socket.emit("setStatus", d);
          })
          .catch(function(e){
              console.log("\nFailed to update the clients with SMS status.\nREASON:/n" + err.message );
          })
          
    });
    
      //! Handle basic client connection
      socket.on("online",function(d){    
        socket.username = d.username;
        socket.token    = d.token;
        onlineClients.push(socket.username);
        c_log(`\n${socket.username} just connected.`.succ);
        socket.broadcast.emit("onlineClients",onlineClients);
      })
      
      //! HANDLE THE DISCONNECTION OF A CLIENT
      socket.on("disconnect", function(d){

        if(socket.username){
          var index = onlineClients.indexOf(socket.username);
          if (index > -1) {
              onlineClients.splice(index, 1);
          }
          c_log(`\n${socket.username} just disconnected`.err);
          socket.broadcast.emit("onlineClients",onlineClients);
        }else{
          
        }
        connUsers--;
        console.log("\nConnected to ".info + connUsers + " client".info + ((connUsers > 1 || connUsers == 0 ) ? "s.".info : ".".info)  + "\t -".err );
          
      });
      
      //! HANDLE REQUESTS FOR A LIST OF ONLINE CLIENTS
      socket.on("getOnline",function(){
        socket.emit("onlineClients",onlineClients);
      });

      //! REMOTE LOGGING FUNCTION
      socket.on("log", function(logData){
        console.log("\n\nNEW LOG REQUEST:\n".yell + str(logData) + "\n\n" );
      });

      //! TEST SOCKET FUNCTION 
      socket.on("test", function(data){
        socket.emit("testEcho", data );
      });
      
      //! HANDLE THE SENDING OF {AN} SMS {MESSAGE}S
      socket.on("sendSMS", function(smsData){
        
      var telephone = ( Array.isArray(smsData) ) ? smsData[0].telephone : smsData.telephone;
      //  j_log(smsData)

        console.log("\nAttempting to send message to " + telephone);
        
        schema.SMS.sendSMS( socket, smsData )
        .then(function(data){
            data.socket.emit("sms",data.message);
            io.emit("SMS", data.data );
            // j_log(data)
        })
        .catch(function(data) {
            data.socket.emit("sms", data.message );
            // j_log(data)
        });
        
      });
      
      //! HANDLE SENT SMS MESSAGES
      socket.on("SENT", function( smsParams ){
        
        //console.log("\n\nPROCESSING SENT SMS REQUEST(s) ...".info)
          io.emit("trueSMS",smsParams);
          schema.SMS.sentSMS( {sms: smsParams} )
        /*.then( function(data){
            console.log(data);
          }).catch(function(err){
            console.log(err)
          })*/
          
          
      });
      
      //! HANDLE FAILED SMS MESSAGES
      socket.on("FAILED", function(msgParams){
        io.emit("falseSMS",msgParams);
        console.log("FAILED TO SEND THE SMS MESSAGE\n".err );
        j_log((msgParams));
      });
      
      //---- CLIENT REGISTRATION
      //! HANDLE CLIENT REGISTRATION
      socket.on("newClient", function(clientParams){
        
        console.log("\nCreating the client " + clientParams.username );
        
        schema.SMS.newClient( socket, clientParams )
        .then(function(data){
          data.socket.emit("clientResponse", makeResponse(200,data.message) );
          c_log("Successfully created client")
          j_log(data)
        })
        .catch(function(data){
          data.socket.emit("clientResponse", { response: 500, data: {  message: data.message } } );
          c_log("Failed to create client")
          c_log(data.message);
        });
        
      });
      
    
    //! CHECK WHETHER THE CLIENT HAS BEEN PREVIOUSLY REGISTERED
    socket.on("isRegistered",function(d){
      c_log(d.api.key+" wants to know if they are registered");
      schema.SMSClient.find(d,function(err,results){
          if(err){
            c_log("Something went wrong when trying to authenticate registration.");
            c_log(err.message)
          }else{
            if(results.length > 0){
              c_log("Authenticating "+d.api.key+"'s request for rememberance")
              socket.emit("expressAuth",results);
            }
          }
      });
    })


    });

    //** SETUP THE STATIC DIRECTORY
    app.use( express.static( __dirname + "/" ) );

    //** SERVE THE SMS SENDING PAGE
    app.route("/").
    get(function(req,res){
        res.sendFile(__dirname + '/index.html');
    });

    //** THE REST API SMS HANDLER
    app.route('/sms')
    .all(function(req, res){
      
        var bodyParams = ( (Object.keys(req.body).length > 0)?req.body:req.query );
        
        //!PERFORM BASIC AUTHENTICATION
        //******to do*******//
        //schema.User.login({bodyParams.auth})
        
        var smsMessage = bodyParams.body || bodyParams;
        
        schema.SMS.testSMS(smsMessage)
        .then(function(smsMessage){
            
            schema.SMS.sendSMS({},smsMessage)
            .then(function(data){
                io.emit("SMS", data.data );
                log(`SMS sent ${data}`.succ)
                res.json( makeResponse( "SUCCESS", data, "next") ); 
            })
            .catch(function(err){
              log(`SMS sending error ${error}`.err);
              res.json( makeResponse( "ERROR", err.message ) ); 
            })
            
        })
        .catch(function(err){
          console.log(err)
          res.json( makeResponse( "ERROR", err ) ); 
        });
          
    });


    //** HOOK THE SERVER TO A PORT
    server.listen(app.port, function(){
      console.log(`The sms service has been started at `.succ + `http://${myAddr}:${app.port}`);
    });
        
});
//** EO MONGODB CONNECTION