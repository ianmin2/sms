//SOCKET CONNECTION CONFIGURATION
var port	= "3000";
var ip		= "41.89.162.252" || "127.0.0.1"; //  //192.168.43.164
// var ip        = "192.168.1.136"
var server_ip = "http://" + ip + ":" + port;
// var server_ip = "http://41.215.129.41:8080";

var clone = function(obj){

    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

var app = angular.module("bixbyte",["ionic","ngCordova","jsonFormatter","ngStorage"]);

app.run([ "$ionicPlatform", "$localStorage", "$rootScope",function( $ionicPlatform, $localStorage, $rootScope ) {

  $rootScope.storage = $localStorage;

  $ionicPlatform.ready(function() {

    if(window.cordova && window.cordova.plugins.Keyboard) {

      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      
      //** ENABLE THE APPLICATION TO START ON BOOT
      cordova.plugins.autoStart.enable();
      
      //** PREVENT THE APPLICATION FROM DIMMING THE SCREEN
      window.plugins.insomnia.keepAwake();
      
      //** PREVENT THE APPLICATION FROM DYING WHEN PLACED IN THE BACKGROUND
      cordova.plugins.backgroundMode.enable();

      //** SET THE APPLICATION BACKGROUND MODE DEFAULTS
      cordova.plugins.backgroundMode.setDefaults({
            title:  "Bixbyte SMS",
            ticker: "Initializing SMS service",
            text:   "Initializing network SMS service."
      });

       //@ BASIC APP DATA INITIALIZATION
        $rootScope.data            = {};
        $rootScope.data.setup      = {};
        $rootScope.data.setup.api  = {};        
        
        //! INITIALIZE THE DEVICE SPECIFIC API KEY
        if( !$rootScope.storage.apiKey ){
            window.plugins.imei.get(
                function(imei) {
                   
                   //! CHECK IF THIS DEVICE IS ALREADY REGISTERED
                    $rootScope.data.setup.api.key  = imei;
                    $rootScope.storage.apiKey      = imei;
                    $rootScope.$apply();
                },
                function() {
                    console.log("error loading imei");
                }
            );
        }else{
            $rootScope.data.setup.api.key   = $rootScope.storage.apiKey;
        }
        


      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);

    };




    if(window.StatusBar) {

        //StatusBar.styleDefault();

        //** HIDE THE STATUSBAR
        StatusBar.styleHidden();

    }

  });

}]);

//document.addEventListener("deviceready", function () {
	
	app.controller("framifySMS", ["$scope","$cordovaSms","$ionicModal", function($scope, $cordovaSms,$ionicModal) {
         
        var ensureSignup =  setInterval(function(){
           if( !$scope.storage.setup ){

                $scope.modal.show();

            }
        },120000);

        //@ ESTABLISH SIGNUP MODAL CREDENTIALS
        $ionicModal.fromTemplateUrl('my-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            console.log("Modal loaded!");
            
            if( !$scope.storage.setup ){

                $scope.modal.show();

            }else{

                //! Do Something else here 
                clearInterval(ensureSignup);

            }
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function() {
            // Execute action

        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function() {
            // Execute action
        });


        document.addEventListener("online", function(c_data){
    
            console.log( "\n@sms\nThe application went online.\n" );

            //** UPDATE THE APPLICATION CONNECTION STATUS
            cordova.plugins.backgroundMode.configure({
                title: "Online!",   
                text: "Trying to estaablish a socket connection."                
            });
    
        //EO ON CONNECT
        }, false );
        
        $scope.sock = io.connect(server_ip);
            
            //!SETUP THE APP VARIABLES
            $scope.app = {};
            $scope.sms = {};
            $scope.sms.sent = [];
            $scope.sms.failed = [];
                    
            //!HANDLE ALERTS IN THE SMS CONTROLLER SCOPE (CONTEXT) 
            $scope.alert = function(pos){
                alert(pos);
            };
            
            //!SENT SMS HANDLER
            $scope.sent = function(data){
                
                //sock.emit("log", data );
                $scope.sock.emit("SENT", data );
                // console.log("\n\t@sms\n\tSuccessfully sent an SMS " + data);
                $scope.sock.emit("log", "SUCCESSFULLY SENT THE MESSAGE");
                $scope.sms.sent.push(data);
                $scope.$apply();

            };
            
            // //!FAILED  SMS HANDLER
            $scope.failed = function(data){
                
                //sock.emit("log", data );
                // console.log("\n\t@sms\n\tFailed to send the specified sms " + data)
                $scope.sock.emit("FAILED", data);
                $scope.sock.emit("log", "FAILED TO SEND THE SMS REASON: "+data);
                $scope.sms.failed.push(data);
                $scope.$apply();
                
            };
        
           
            
            //** CAPTURE PREVIOUS SMS RECORDS
            $scope.sock.on("setStatus", function(data){

                // console.dir(data)

                if( data.response === ( 200 || "SUCCESS" ) ){
                    //@ HANDLE PROPER SERVER RESULTS

                    // console.log("\n\t@sms\nSMS sending status lists received.\n\n")
                    
                    //@ UPDATE THE UI WITH THE SERVER DATA
                    $scope.sms.sent     = data.data.message.sent;

                    $scope.sms.failed   = data.data.message.unsent

                    //@ APPLY THE CHANGES 
                    $scope.$apply();

                    //@ TRY RE-SENDING THE FAILED MESSAGES
                    $scope.sms.failed.forEach(function(fsms){
                      $scope.sendSMS( fsms.sms, $scope.sent,$scope.failed );
                    });
                                                
                }else{
                    //@ HANDLE MESSED UP SERVER RESULTS

                    alert( data.data.message );
                    
                }
                
            });
                    
            
            //** HANDLE SOCKET SMS NOTIFICATIONS
            $scope.sock.on("SMS", function(data){
                //=> .message
                //=> .telephone
                
                //! VERIFY THE SINGULARITY OR MULTIPLICITY OF REQUESTS
                if( Array.isArray(data) ){
                    
                    //@ CONFIRM RECEIPT WITH THE SERVER
                    $scope.sock.emit("log", "\nAttempting to send " + ( data.length ) + "  messages.\n" );
 
                    // console.log("\n\t@sms\n\tReceived a multiple sms sending request for " + data.length + " messages ");

                    //@ HANDLE THE SENDING OF EACH MESSAGE
                    for( sms in data ){
                        
                        //@ CHECK IF THE MESSAGES ARE MEANT FOR THIS DEVICE
                        if( data[sms].password == $scope.storage.apiKey ){
                            
                            //& THE ACTUAL SENDING
                            $scope.sendSMS( data[sms], $scope.sent, $scope.failed );
                            // console.log("\n\t@sms\n\t\n");

                        };                        
                        
                    };
                    
                }else{
                
                    //@ CONFIRM RECEIPT WITH THE SERVER
                   $scope.sock.emit("log", "Attempting to send a single message.");

                //    console.log("\n\t@sms\n\tReceived a single sms sending request.");

                   //& THE ACTUAL SENDING
                   $scope.sendSMS( data, $scope.sent, $scope.failed );
                    
                };
    
                //@ SHOW MOST RECENT MESSAGES FIRST (regardles of sent status)
                $('html, body').animate({
                    scrollTop: $("#bottom").offset().top
                }, 1000);
                
            });
            
            
            //** HANDLE SMS SENDING
            $scope.sendSMS = function(msgData, succ_callback, err_callback){
                
                //@ ENSURE THAT ONLY AUTHORIZED MESSAGES ARE SENT
                if( msgData.password==$scope.storage.apiKey){

                    var del_index = $scope.sms.failed.indexOf( msgData );

                    if( msgData._id ){
                        msgData = msgData.sms;
                    };

                    //& SEND THE ACTUAL MESSAGE
                    $cordovaSms
                    .send(msgData.telephone, msgData.message, {} )
                    .then(function(mess) {
                        
                        //@ DELETE THE ITEM FROM THE UNSENT MESSAGES LIST                    
                        if(del_index > -1){
                            $scope.sms.failed.splice(del_index, 1);
                            $scope.$apply();
                        }

                        //@ HANDLE SUCCESSFUL SMS SENDING REQUESTS
                        succ_callback(msgData);
                        
                        //@ UPDATE THE STATUSBAR MESSAGE 
                        cordova.plugins.backgroundMode.configure({
                            title: "Bixbyte SMS",   
                            text: "SMS sent to " + msgData.telephone + "\n\nWaiting for more ..."                
                        });

                    }, function(error) {
                    
                        //@ HANDLE FAILED SMS SENDING REQUESTS
                        err_callback(error);

                        //@ UPDATE THE STATUSBAR MESSAGE
                        cordova.plugins.backgroundMode.configure({
                            title: "Bixbyte SMS",   
                            text: "Couldn't send to " + msgData.telephone + "\n\nAirtime? ..."                
                        });
                        
                    });                    

                }
                
            };

            //** HANDLE THE CLIENT APP REGISTRATION
             $scope.registerApp = function(){
                $scope.sock.emit("newClient",$scope.data.setup);
            };
            
            //! HANDLE CLIENT REGISTRATION REQUEST RESPONSES
            $scope.sock.on("clientResponse",function(d){
                
                if(d.response == 200){
                    $scope.storage.setup = $scope.data.setup;
                    clearInterval(ensureSignup);
                    $scope.modal.hide();
                    alert("This device has successfully been registered.");
                    $scope.modal.remove();
                }else{
                    alert(d.message);
                }

            });

            //@ HANDLE EXPRESS AUTHENTICATION DIRECTIVES
            $scope.sock.on("expressAuth",function(d){
               $scope.storage.setup = d;               
               $scope.modal.hide(); 
               clearInterval(ensureSignup); 
               alert("This device has been remembered!");            
               $scope.modal.remove(); 
            })

            //** HANDLE INITIAL CLIENT CONNECTION
            $scope.sock.on("connect",function(){
               
                // console.log("\n@sms\nEstablished a connection to the bixbyte SMS server.")

                if( !$scope.storage.setup ){
                        $scope.sock.emit("isRegistered", {api: { key: $scope.storage.apiKey} });
                }else{
                        $scope.sock.emit("online",{ token:$scope.storage.apiKey, username: $scope.storage.setup[0].username });
                }

               //@ FETCH THE UPDATED LIST OF MESSAGES 
                $scope.sock.emit("getStatus");

                //@ UPDATE THE STATUSBAR MESSAGE
                cordova.plugins.backgroundMode.configure({
                    title: "Connected!",   
                    text: "Watching for SMS Messages to send. "                
                });

               
            });

            //** HANDLE CLIENT DISCONNECTION
            $scope.sock.on("disconnect", function(){
                // console.log("\n\t@sms\n\tDisconnected from the server!");

                // if($scope.storage.setup){
                //     $scope.sock.emit("offline",{ token:$scope.storage.apiKey, username: $scope.storage });
                // }

                //@ UPDATE THE STATUSBAR MESSAGE
                cordova.plugins.backgroundMode.configure({
                    title: "Disconnected!",   
                    text: "Any SMS messages sent right now will not be sent!\nPlease connect to the internet to ensure continuity of the service."                
                });

            });
            
            //** HANDLE CLIENT RECONNECTION
            $scope.sock.on("reconnect", function(){
                // console.log("\n\t@sms\n\tReconnected to the server!");

                //@ FETCH THE UPDATED LIST OF MESSAGES 
                $scope.sock.emit("getStatus");

                //@ UPDATE THE STATUSBAR MESSAGE
                cordova.plugins.backgroundMode.configure({
                    title: "Connected!",   
                    text: "Watching for SMS Messages to send. "                
                });

            });
        
        
   
	}]);
    
//}, false);
